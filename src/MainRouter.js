import PropTypes from 'prop-types';
import React, { Component } from 'react';
import App from './App';

export default class MainRouter extends Component {
  constructor() {
    super();
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 304
      }
    };
  }

  getChildContext() {
    return {
      navOpenState: this.state.navOpenState
    };
  }
  onNavResize = navOpenState => {
    this.setState({
      navOpenState
    });
  };

  render() {
    return <App onNavResize={this.onNavResize} {...this.props} />;
  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object
};
