import React, { Component } from 'react';
import Avatar from '@atlaskit/avatar';

// Lodash Functions
import _without from 'lodash/without';
import _size from 'lodash/size';
import _truncate from 'lodash/truncate';

// Utils
import DataUtils from '../data/dataUtils';
import utils from '../utils';

class ConversationItem extends Component {
	render() {
		const { conv, selfId, active } = this.props;
		const memberId = _without(conv.members, selfId)[0];
		const member = DataUtils.getOneUser(memberId);
		const messages = DataUtils.getConvMessages(conv.id);
		const lastMessage = _size(messages) > 0 ? messages[0] : undefined;

		return (
			<div
				onClick={this.props.handleConvClicked.bind(this, conv)}
				className={`flex-container flex-v-center pointer conv-item ${
					active ? 'active' : ''
				}`}>
				<div className="conv-avatar">
					<Avatar src={member.avatars[0].url} />
				</div>
				<div className="conv-content">
					<div className="flex-container flex-v-center">
						<div>{member.name}</div>
						<span className="flex-expand" />
						<div className="text-muted text-smaller flex-pull-right">
							{lastMessage && utils.prettyDate(lastMessage.created_at)}
						</div>
					</div>
					<div className="text-muted text-small">
						{lastMessage && lastMessage.text ? (
							_truncate(lastMessage.text, { length: 20 })
						) : (
							<span>&nbsp;</span>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default ConversationItem;
