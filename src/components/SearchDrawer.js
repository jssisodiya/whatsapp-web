import PropTypes from 'prop-types';
import React, { Component } from 'react';

// Components
import SearchResults from './SearchResults';

// Utils
import DataUtils from '../data/dataUtils';

export default class SearchDrawer extends Component {
  static propTypes = {
    onResultClicked: PropTypes.func,
    onSearchInputRef: PropTypes.func
  };

  state = {
    searchString: '',
    results: DataUtils.getFriends(this.props.selfId)
  };

  filterChange = () => {
    this.setState({
      searchString: this.searchInput.value
    });
  };

  searchResults = () => {
    const { results, searchString } = this.state;

    const matchingResults = results.filter(
      c => c.name.toLowerCase().indexOf(searchString.toLowerCase()) >= 0
    );

    return (
      <SearchResults
        matchingResults={matchingResults}
        onResultClicked={result => {
          this.props.onResultClicked(result);
          this.searchInput.value = '';
          this.filterChange();
        }}
      />
    );
  };

  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="Search..."
          onKeyUp={this.filterChange}
          ref={el => {
            this.searchInput = el;
            if (this.props.onSearchInputRef) {
              this.props.onSearchInputRef(el);
            }
          }}
          style={{
            border: 'none',
            display: 'block',
            fontSize: 24,
            fontWeight: 200,
            outline: 'none',
            padding: '0 0 0 12px'
          }}
        />
        {this.searchResults()}
      </div>
    );
  }
}
