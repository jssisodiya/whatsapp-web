import React, { Component } from 'react';

// Lodash Functions
import _map from 'lodash/map';
import _size from 'lodash/size';

// Utils
import Utils from '../utils';

class MessagesContainer extends Component {
	render() {
		const { currentConvId, messages, selfId, convs } = this.props;
		return (
			<div className="message-container" style={{ padding: '50px' }}>
				{!currentConvId &&
					_size(convs) > 0 && (
						<h3 style={{ textAlign: 'center' }}>
							Click on any conversation to view the messages
						</h3>
					)}
				{_map(messages, message => {
					let direction = message.author_id === selfId ? 'out' : 'in';
					return (
						<div className="message-row">
							<span className={`message message-${direction}`}>
								{message.text}
								<span className="time">
									{Utils.prettyDate(message.created_at)}
								</span>
							</span>
						</div>
					);
					return <div>{message.text}</div>;
				})}
				<div className="message-tile" />
			</div>
		);
	}
}

export default MessagesContainer;
