import React, { Component } from 'react';

class InputContainer extends Component {
	handleNewMessage = e => {
		let text = e.target.querySelectorAll('input')[0].value;
		if (text !== '') {
			this.props.handleNewMessage(text);
			e.target.reset();
		}
		e.preventDefault();
	};

	render() {
		return (
			<div className="input-row flex-container flex-expand">
				<form
					onSubmit={this.handleNewMessage}
					className="flex-container flex-expand">
					<input
						className="input-field flex-expand"
						type="text"
						placeholder="Type a message"
					/>
				</form>
			</div>
		);
	}
}

export default InputContainer;
