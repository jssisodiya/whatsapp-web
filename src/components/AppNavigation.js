import PropTypes from 'prop-types';
import React from 'react';
import Nav, {
  AkContainerTitle,
  AkSearchDrawer,
  presetThemes,
  createGlobalTheme
} from '@atlaskit/navigation';
import SearchIcon from '@atlaskit/icon/glyph/search';
import ArrowleftIcon from '@atlaskit/icon/glyph/arrow-left';
import SignOutIcon from '@atlaskit/icon/glyph/sign-out';

// Lodash Functions
import _map from 'lodash/map';
import _size from 'lodash/size';

// Components
import SearchDrawer from '../components/SearchDrawer';
import ConversationItem from '../components/ConversationItem';

import whatsAppLogo from '../img/WhatsApp_Logo.png';

// Utils
import DataUtils from '../data/dataUtils';

export default class AppNavigation extends React.Component {
  state = {
    openDrawer: ''
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object
  };

  componentDidMount() {
    if (_size(this.props.convs) === 0) {
      this.openDrawer('search');
    }
  }

  openDrawer = openDrawer => {
    this.setState({ openDrawer });
  };

  render() {
    const { convs, selfId } = this.props;
    const selfObject = DataUtils.getOneUser(selfId);
    const backIcon = <ArrowleftIcon label="Back icon" size="medium" />;
    const globalPrimaryIcon = (
      <img
        alt="WhatsApp"
        width={40}
        height={40}
        style={{ borderRadius: '50%' }}
        src={whatsAppLogo}
      />
    );

    return (
      <Nav
        globalTheme={{
          ...presetThemes.global,
          ...createGlobalTheme('#ffffff', '#33D16B')
        }}
        containerTheme={{
          ...presetThemes.global,
          ...createGlobalTheme('#42526E', '#ffffff')
        }}
        isOpen={this.context.navOpenState.isOpen}
        width={this.context.navOpenState.width}
        onResize={this.props.onNavResize}
        containerHeaderComponent={() => (
          <AkContainerTitle
            icon={<img alt={selfObject.name} src={selfObject.avatars[0].url} />}
            text={selfObject.name}
          />
        )}
        globalPrimaryIcon={globalPrimaryIcon}
        globalPrimaryItemHref="/"
        globalSearchIcon={<SearchIcon label="Search" />}
        globalCreateIcon={<SignOutIcon label="Sign out" />}
        hasBlanket
        drawers={[
          <AkSearchDrawer
            backIcon={backIcon}
            isOpen={this.state.openDrawer === 'search'}
            key="search"
            onBackButton={() => this.openDrawer(null)}
            primaryIcon={globalPrimaryIcon}>
            <SearchDrawer
              selfId={this.props.selfId}
              onResultClicked={result => {
                this.props.handleUserClicked(result);
                this.openDrawer(null);
              }}
              onSearchInputRef={ref => {
                this.searchInputRef = ref;
              }}
            />
          </AkSearchDrawer>
        ]}
        onSearchDrawerOpen={() => this.openDrawer('search')}
        onCreateDrawerOpen={() => this.props.handleLogout()}>
        <div
          style={{
            margin: '0px -16px 0px -16px',
            borderTop: '1px solid #dbdbd3'
          }}>
          {_map(convs, conv => (
            <ConversationItem
              conv={conv}
              selfId={selfId}
              active={conv.id === this.props.currentConvId}
              handleConvClicked={this.props.handleConvClicked}
            />
          ))}
        </div>
      </Nav>
    );
  }
}
