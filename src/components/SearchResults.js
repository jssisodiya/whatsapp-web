import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Avatar from '@atlaskit/avatar';

// Lodash Functions
import _map from 'lodash/map';

export default class SearchResults extends Component {
  static propTypes = {
    matchingResults: PropTypes.arrayOf(PropTypes.object),
    onResultClicked: PropTypes.func
  };

  render() {
    if (!this.props.matchingResults.length) {
      return <p>Nothing found, keep on searching!</p>;
    }

    return (
      <ul
        style={{
          listStyleType: 'none',
          margin: '16px 0',
          padding: 0
        }}>
        {_map(this.props.matchingResults, result => (
          <div
            onClick={this.props.onResultClicked.bind(this, result)}
            className="flex-container flex-v-center pointer"
            style={{ margin: '4px 0px' }}>
            <div style={{ margin: '4px 8px' }}>
              <Avatar src={result.avatars[0].url} />
            </div>
            <div>{result.name}</div>
          </div>
        ))}
      </ul>
    );
  }
}
