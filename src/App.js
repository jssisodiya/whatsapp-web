import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@atlaskit/avatar';
import Modal from '@atlaskit/modal-dialog';
import Page from '@atlaskit/page';

import _map from 'lodash/map';
import _without from 'lodash/without';
import _size from 'lodash/size';
import _orderBy from 'lodash/orderBy';
import _chunk from 'lodash/chunk';

import './App.css';

import AppNavigation from './components/AppNavigation';
import InputContainer from './components/InputContainer';
import MessagesContainer from './components/MessagesContainer';

import DataUtils from './data/dataUtils';

class App extends Component {
  state = {
    currentConvId: undefined,
    selfId: undefined,
    isModalOpen: false
  };
  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func
  };
  componentDidMount() {
    const selfId = DataUtils.getLoggedInUserId();
    if (!selfId) {
      this.setState({ isModalOpen: true });
    } else {
      this.setState({ selfId });
    }
    if (window && window.firebase) {
      this.listenForMessages();
    }
  }
  hideModal = () => {
    this.setState({ isModalOpen: false });
  };
  handleLogin = () => {
    DataUtils.setLoggedInUser(this.state.selectedUser);
    window.location.reload();
  };
  handleLogout = () => {
    if (window.confirm('Are you sure, you want to logout?')) {
      DataUtils.setLoggedInUser({});
      window.location.reload();
    }
  };
  listenForMessages = () => {
    const db = window.firebase.database();
    const chat = db.ref('chat');
    chat.on('value', snapshot => {
      const { conv, message } = snapshot.val();
      if (message && message.author_id !== this.state.selfId) {
        DataUtils.setNewMessage(conv, message);
        this.forceUpdate();
      }
    });
  };
  signalFirebase = (conv, message) => {
    const db = window.firebase.database();
    const convsRef = db.ref('chat');
    convsRef.set({
      conv,
      message
    });
  };
  handleNewMessage = text => {
    const { currentConvId, selfId } = this.state;
    if (currentConvId && text !== '') {
      const { conv, message } = DataUtils.createMessage(
        currentConvId,
        text,
        selfId
      );
      if (conv && message && window.firebase) {
        this.signalFirebase(conv, message);
      }
      this.forceUpdate();
    }
  };
  handleUserClicked = selectedUser => {
    const selectedUserId = selectedUser.id;
    let conv = DataUtils.getConvForMembers([this.state.selfId, selectedUserId]);
    if (!conv) {
      const chatData = DataUtils.createConversation([
        this.state.selfId,
        selectedUserId
      ]);
      conv = chatData.conv;
      this.forceUpdate();
    }
    this.setState({ currentConvId: conv.id });
  };
  handleConvClicked = conv => {
    this.setState({ currentConvId: conv.id });
  };
  render() {
    const { currentConvId, selfId } = this.state;
    const friends = DataUtils.getFriends(selfId);
    const allUsers = DataUtils.getAllUsers();
    const allUsersChunk = _chunk(
      _orderBy(allUsers, 'name'),
      _size(allUsers) / 2
    );
    const convs = DataUtils.getUserConversations(selfId);

    let currentConv;
    let messages = [];
    let otherUser;
    if (currentConvId) {
      currentConv = DataUtils.getOneConversation(currentConvId);
      messages = DataUtils.getConvMessages(currentConvId);
      const otherUserId = _without(currentConv.members, selfId)[0];
      otherUser = DataUtils.getOneUser(otherUserId);
    }

    return (
      <div className="flex-container" style={{ height: '100%' }}>
        {selfId && (
          <Page
            navigationWidth={200}
            navigation={
              <AppNavigation
                convs={convs}
                friends={friends}
                selfId={selfId}
                currentConvId={currentConvId}
                handleUserClicked={this.handleUserClicked}
                handleConvClicked={this.handleConvClicked}
                handleLogout={this.handleLogout}
              />
            }>
            <div style={{ width: '100%' }}>
              <div className="right-panel">
                {currentConvId && (
                  <div className="conv-header">
                    {otherUser && (
                      <div className="flex-container flex-v-center">
                        <div style={{ margin: '4px 8px' }}>
                          <Avatar src={otherUser.avatars[0].url} />
                        </div>
                        <div>{otherUser.name}</div>
                      </div>
                    )}
                  </div>
                )}
                <MessagesContainer
                  currentConvId={currentConvId}
                  messages={messages}
                  selfId={selfId}
                  convs={convs}
                />
                {currentConvId && (
                  <InputContainer handleNewMessage={this.handleNewMessage} />
                )}
              </div>
            </div>
          </Page>
        )}
        {this.state.isModalOpen && (
          <Modal
            heading="Login as ?"
            actions={
              this.state.selectedUser
                ? [
                    {
                      text: 'Login',
                      onClick: this.handleLogin
                    }
                  ]
                : []
            }
            shouldCloseOnEscapePress={false}
            shouldCloseOnOverlayClick={false}
            onClose={this.state.selectedUser ? this.hideModal : null}>
            <div className="flex-container">
              {_map(allUsersChunk, users => {
                return (
                  <div className="flex-container flex-wrap flex-expand">
                    {_map(users, user => {
                      return (
                        <div
                          onClick={() => {
                            this.setState({ selectedUser: user });
                          }}
                          className={`flex-container flex-v-center user-selection ${
                            this.state.selectedUser &&
                            this.state.selectedUser.id === user.id
                              ? 'active'
                              : ''
                          }`}
                          style={{ margin: '4px' }}>
                          <Avatar src={user.avatars[0].url} />
                          <div style={{ padding: '0px 4px' }}>{user.name}</div>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </div>
          </Modal>
        )}
      </div>
    );
  }
}

export default App;
