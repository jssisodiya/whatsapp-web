export default [
	{
		id: 1,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/03F55412-DE8A-4F83-AAA6-D67EE5CE48DA-200w.jpeg',
				width: 200
			}
		],
		name: 'Abagael Raychard',
		gender: 'female'
	},
	{
		id: 2,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/2DDDE973-40EC-4004-ABC0-73FD4CD6D042-200w.jpeg',
				width: 200
			}
		],
		name: 'Dugan Drawe',
		gender: 'male'
	},
	{
		id: 3,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/5F8C5D50-DDB6-4F06-AA15-ACB30D8D910D-200w.jpeg',
				width: 200
			}
		],
		name: 'Rawley Spraker',
		gender: 'male'
	},
	{
		id: 4,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/E0B4CAB3-F491-4322-BEF2-208B46748D4A-200w.jpeg',
				width: 200
			}
		],
		name: 'Russ Matos',
		gender: 'male'
	},
	{
		id: 5,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/7E570445-25F0-4276-8E8F-084ABA2C8FB8-200w.jpeg',
				width: 200
			}
		],
		name: 'Cameron Langfitt',
		gender: 'male'
	},
	{
		id: 6,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/BA0CB1F2-8C79-4376-B13B-DD5FB8772537-200w.jpeg',
				width: 200
			}
		],
		name: 'Thaddus Saucer',
		gender: 'male'
	},
	{
		id: 7,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/FBEBF655-4886-455A-A4A4-D62B77DD419B-200w.jpeg',
				width: 200
			}
		],
		name: 'Derby Hilz',
		gender: 'male'
	},
	{
		id: 8,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/344CFC24-61FB-426C-B3D1-CAD5BCBD3209-200w.jpeg',
				width: 200
			}
		],
		name: 'Sherman Rell',
		gender: 'male'
	},
	{
		id: 9,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/A7299C8E-CEFC-47D9-939A-3C8CA0EA4D13-200w.jpeg',
				width: 200
			}
		],
		name: 'Abagael Merilos',
		gender: 'female'
	},
	{
		id: 10,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/26CFEFB3-21C8-49FC-8C19-8E6A62B6D2E0-200w.jpeg',
				width: 200
			}
		],
		name: 'Tome Suminski',
		gender: 'male'
	},
	{
		id: 11,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/B3CF5288-34B0-4A5E-9877-5965522529D6-200w.jpeg',
				width: 200
			}
		],
		name: 'Sholom Laguerre',
		gender: 'male'
	},
	{
		id: 12,
		avatars: [
			{
				height: 199,
				size: 'small',
				url: '/avatars/AEF44435-B547-4B84-A2AE-887DFAEE6DDF-200w.jpeg',
				width: 200
			}
		],
		name: 'Jon Hsun',
		gender: 'male'
	},
	{
		id: 13,
		avatars: [
			{
				height: 133,
				size: 'small',
				url: '/avatars/1C4EEDC2-FE9C-40B3-A2C9-A038873EE692-200w.jpeg',
				width: 200
			}
		],
		name: 'Gilburt Veys',
		gender: 'male'
	},
	{
		id: 14,
		avatars: [
			{
				height: 133,
				size: 'small',
				url: '/avatars/282A12CA-E0D7-4011-8BDD-1FAFAAB035F7-200w.jpeg',
				width: 200
			}
		],
		name: 'Udell Sterlace',
		gender: 'male'
	},
	{
		id: 15,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/852EC6E1-347C-4187-9D42-DF264CCF17BF-200w.jpeg',
				width: 200
			}
		],
		name: 'Herby Mcquiddy',
		gender: 'male'
	},
	{
		id: 16,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/852EC6E1-347C-4187-9D42-DF264CCF17BF-200w.jpeg',
				width: 200
			}
		],
		name: 'Niles Vandy',
		gender: 'male'
	},
	{
		id: 17,
		avatars: [
			{
				height: 133,
				size: 'small',
				url: '/avatars/1C4EEDC2-FE9C-40B3-A2C9-A038873EE692-200w.jpeg',
				width: 200
			}
		],
		name: 'Alessandro Arlington',
		gender: 'male'
	},
	{
		id: 18,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/B3CF5288-34B0-4A5E-9877-5965522529D6-200w.jpeg',
				width: 200
			}
		],
		name: 'Harcourt Jeanpierre',
		gender: 'male'
	},
	{
		id: 19,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/7E570445-25F0-4276-8E8F-084ABA2C8FB8-200w.jpeg',
				width: 200
			}
		],
		name: 'Crawford Buzick',
		gender: 'male'
	},
	{
		id: 20,
		avatars: [
			{
				height: 133,
				size: 'small',
				url: '/avatars/282A12CA-E0D7-4011-8BDD-1FAFAAB035F7-200w.jpeg',
				width: 200
			}
		],
		name: 'Omar Kumfer',
		gender: 'male'
	},
	{
		id: 21,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/5F8C5D50-DDB6-4F06-AA15-ACB30D8D910D-200w.jpeg',
				width: 200
			}
		],
		name: 'Kelvin Roddey',
		gender: 'male'
	},
	{
		id: 22,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/A7299C8E-CEFC-47D9-939A-3C8CA0EA4D13-200w.jpeg',
				width: 200
			}
		],
		name: 'Abagael Coats',
		gender: 'female'
	},
	{
		id: 23,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/BA0CB1F2-8C79-4376-B13B-DD5FB8772537-200w.jpeg',
				width: 200
			}
		],
		name: 'Lindsay Gonzaliz',
		gender: 'male'
	},
	{
		id: 24,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/E0B4CAB3-F491-4322-BEF2-208B46748D4A-200w.jpeg',
				width: 200
			}
		],
		name: 'Spike Rinker',
		gender: 'male'
	},
	{
		id: 25,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/03F55412-DE8A-4F83-AAA6-D67EE5CE48DA-200w.jpeg',
				width: 200
			}
		],
		name: 'Candice Kloer',
		gender: 'female'
	},
	{
		id: 26,
		avatars: [
			{
				height: 246,
				size: 'small',
				url: '/avatars/344CFC24-61FB-426C-B3D1-CAD5BCBD3209-200w.jpeg',
				width: 200
			}
		],
		name: 'Staford Sucharski',
		gender: 'male'
	},
	{
		id: 27,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/FBEBF655-4886-455A-A4A4-D62B77DD419B-200w.jpeg',
				width: 200
			}
		],
		name: 'Patsy Stafford',
		gender: 'male'
	},
	{
		id: 28,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/26CFEFB3-21C8-49FC-8C19-8E6A62B6D2E0-200w.jpeg',
				width: 200
			}
		],
		name: 'Phillip Bartus',
		gender: 'male'
	},
	{
		id: 29,
		avatars: [
			{
				height: 200,
				size: 'small',
				url: '/avatars/2DDDE973-40EC-4004-ABC0-73FD4CD6D042-200w.jpeg',
				width: 200
			}
		],
		name: 'Tod Grantham',
		gender: 'male'
	},
	{
		id: 30,
		avatars: [
			{
				height: 199,
				size: 'small',
				url: '/avatars/AEF44435-B547-4B84-A2AE-887DFAEE6DDF-200w.jpeg',
				width: 200
			}
		],
		name: 'Zary Gennaria',
		gender: 'male'
	}
];
