import lockr from 'lockr';

// Lodash Functions
import _find from 'lodash/find';
import _intersection from 'lodash/intersection';
import _random from 'lodash/random';
import _size from 'lodash/size';
import _filter from 'lodash/filter';
import _indexOf from 'lodash/indexOf';
import _orderBy from 'lodash/orderBy';

import users from './users';

function getAllConversations() {
	return lockr.get('conversations') || {};
}
function getAllMessages() {
	return lockr.get('messages') || {};
}

class DataUtils {
	static getAllUsers() {
		return users;
	}
	static getOneUser(id) {
		return _find(users, u => u.id === id);
	}
	static getFriends(selfId) {
		return _filter(users, u => u.id !== selfId);
	}
	static getConvForMembers(members) {
		const allConvs = getAllConversations();
		return _find(
			allConvs,
			c => _size(members) === _size(_intersection(members, c.members))
		);
	}
	static createConversation(members, message = undefined) {
		if (_size(members) < 2) {
			return null;
		}
		let conv = {
			id: _random(1, 100000),
			members,
			created_at: new Date().getTime(),
			updated_at: new Date().getTime()
		};

		let allConvs = getAllConversations();
		allConvs[conv.id] = conv;
		lockr.set('conversations', allConvs);

		if (message) {
			message.id = _random(1, 100000);
			message.conv_id = conv.id;
			message.created_at = new Date().getTime();
			conv.updated_at = new Date().getTime();
			let allMessages = getAllMessages();
			allMessages[message.id] = message;
			lockr.set('messages', allMessages);
		}

		return { conv, message };
	}
	static getUserConversations(selfId) {
		return _orderBy(
			_filter(getAllConversations(), c => _indexOf(c.members, selfId) > -1),
			'updated_at',
			'desc'
		);
	}
	static getOneConversation(id) {
		return getAllConversations()[id];
	}
	static getConvMessages(convId) {
		const allMessages = getAllMessages();
		return _orderBy(
			_filter(allMessages, m => m.conv_id === convId),
			'created_at',
			'desc'
		);
	}
	static createMessage(convId, text, authorId) {
		let message = {
			id: _random(1, 100000),
			created_at: new Date().getTime(),
			text,
			author_id: authorId,
			conv_id: convId
		};
		let allMessages = getAllMessages();
		allMessages[message.id] = message;

		let allConvs = getAllConversations();
		let conv = allConvs[convId];
		conv.updated_at = new Date().getTime();
		allConvs[convId] = conv;

		lockr.set('conversations', allConvs);
		lockr.set('messages', allMessages);
		return { conv, message };
	}
	static setNewMessage(conv, message) {
		console.log('setNewMessage', conv, message);
		let allMessages = getAllMessages();
		allMessages[message.id] = message;

		let allConvs = getAllConversations();
		allConvs[conv.id] = conv;
		lockr.set('conversations', allConvs);
		lockr.set('messages', allMessages);
	}
	static setLoggedInUser(user) {
		console.log('setLoggedInUser', user);
		lockr.set('loggedInUserId', user.id);
	}
	static getLoggedInUserId() {
		return lockr.get('loggedInUserId');
	}
}

export default DataUtils;
