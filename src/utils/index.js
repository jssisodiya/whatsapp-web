class Utils {
	static prettyDate(time, suffix = '') {
		const monthNames = [
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'May',
			'Jun',
			'Jul',
			'Aug',
			'Sep',
			'Oct',
			'Nov',
			'Dec'
		];
		if (!time) return '';

		time = new Date(time);
		time = time.getTime() - new Date().getTimezoneOffset() * 60000;
		time = new Date(time);

		time = time.toISOString();
		time = (time || '').replace(/-/g, '/').replace(/[TZ]/g, ' ');
		time = time.trim();
		time = (time || '').split('.')[0];
		var date = new Date((time || '').replace(/-/g, '/').replace(/[TZ]/g, ' '));
		var diff = (new Date().getTime() - date.getTime()) / 1000;
		var day_diff = Math.floor(diff / 86400);

		if (isNaN(day_diff) || day_diff < 0) return;

		return (
			(day_diff === 0 &&
				((diff < 60 && 'just now') ||
					(diff < 120 && `1m ${suffix}`) ||
					(diff < 3600 && Math.floor(diff / 60) + 'm ' + suffix) ||
					(diff < 7200 && '1h ' + suffix) ||
					(diff < 86400 && Math.floor(diff / 3600) + 'h ' + suffix))) ||
			(day_diff === 1 && '1d ' + suffix) ||
			(day_diff < 7 && day_diff + 'd ' + suffix) ||
			(day_diff < 31 && Math.ceil(day_diff / 7) + 'w ' + suffix) ||
			date.getDate() +
				' ' +
				monthNames[date.getMonth()] +
				', ' +
				date.getFullYear()
		);
	}
}

export default Utils;
